﻿USE practica1;

/* Consultas */

-- 1.	Mostrar todos los campos y todos los registros de la tabla empleado.
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no  FROM emple;

-- 2.	Mostrar todos los campos y todos los registros de la tabla departamento.
SELECT dept_no,
       dnombre,
       loc FROM depart;

-- 3.	Mostrar el apellido y oficio de cada empleado.
SELECT DISTINCT apellido,oficio FROM emple;

-- 4.	Mostrar localización y número de cada departamento.
SELECT  loc,dept_no FROM depart;

-- 5.	Mostrar el número, nombre y localización de cada departamento.
SELECT d.dept_no,d.dnombre,d.loc FROM depart d;

-- 6.	Indicar el número de empleados que hay.
SELECT COUNT(*) FROM emple;

-- 7.	Datos de los empleados ordenados por apellido de forma ascendente.
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no FROM emple ORDER BY apellido ASC;

-- 8.	Datos de los empleados ordenados por apellido de forma descendente.
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no FROM emple ORDER BY apellido DESC;

-- 9.	Indicar el número de departamentos que hay.
SELECT COUNT(*) FROM depart;

-- 10. Indicar el número de empleados más el número de departamentos.
SELECT (SELECT COUNT(*) FROM emple) + (SELECT COUNT(*) FROM depart d);

-- 11. Datos de los empleados ordenados por número de departamento descendentemente.
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no FROM emple ORDER BY dept_no DESC;

-- 12. Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente.
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no FROM emple ORDER BY dept_no DESC, oficio ASC;

-- 13. Datos de los empleados ordenados por número de departamento descendentemente 
--     y por apellido ascendentemente.
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no FROM emple ORDER BY dept_no DESC, apellido ASC;

-- 14. Mostrar los códigos de los empleados cuyo salario sea mayor que 2000.
SELECT emp_no FROM emple WHERE salario>2000;

-- 15. Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000.
SELECT emp_no,apellido FROM emple WHERE salario<2000;

-- 16. Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no FROM  emple WHERE salario BETWEEN 1500 AND 2500;

-- 17. Mostrar los datos de los empleados cuyo oficio sea ʻANALISTAʼ.
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no FROM emple WHERE oficio='Analista';

-- 18. Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen más de 2000 €
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no FROM emple WHERE oficio='Analista' AND salario>2000;

-- 19. Seleccionar el apellido y oficio de los empleados del departamento número 20.
SELECT apellido, oficio FROM emple WHERE dept_no=20;

-- 20. Contar el número de empleados cuyo oficio sea VENDEDOR
SELECT COUNT(*) FROM emple WHERE oficio='Vendedor';

-- 21. Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n
--     ordenados por apellido de forma ascendente.
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no FROM emple WHERE apellido LIKE 'M%' OR apellido LIKE 'N%' ORDER BY apellido ASC;

-- 22. Seleccionar los empleados cuyo oficio sea ʻVENDEDORʼ.
-- Mostrar los datos ordenados por apellido de forma ascendente.
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no FROM emple WHERE oficio='Vendedor' ORDER BY apellido ASC;

-- 23. Mostrar los apellidos del empleado que más gana.
  -- C1
  SELECT MAX(salario) FROM emple;
  -- final
  SELECT apellido FROM emple WHERE salario=(SELECT MAX(salario) FROM emple);

-- 24. Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ʻANALISTAʼ. 
--     Ordenar el resultado por apellido y oficio de forma ascendente.
SELECT emp_no,
       apellido,
       oficio,
       dir,
       fecha_alt,
       salario,
       comision,
       dept_no FROM emple WHERE dept_no=10 AND oficio='Analista' ORDER BY apellido,oficio;

-- 25. Realizar un listado de los distintos meses en que los empleados se han dado de alta
SELECT DISTINCT MONTH(fecha_alt) FROM emple;

-- 26. Realizar un listado de los distintos años  en que los empleados se han dado de alta
SELECT DISTINCT YEAR(fecha_alt) FROM emple;

-- 27. Realizar un listado de los distintos días del mes en que los empleados se han dado de alta
SELECT DISTINCT DAYOFMONTH(fecha_alt) FROM emple;

-- 28. Mostrar los apellidos de los empleados que tengan un salario mayor que 2000
--     o que pertenezcan al departamento número 20.
SELECT DISTINCT apellido FROM emple WHERE salario>2000 OR dept_no=20;

-- 29. Realizar un listado donde nos coloque el apellido del empleado y
--     el nombre del departamento al que pertenece
SELECT DISTINCT apellido,d.dnombre FROM emple e JOIN depart d ON e.dept_no = d.dept_no; 

-- 30. Realizar un listado donde nos coloque el apellido del empleado, 
--     el oficio del empleado y el nombre del departamento al que pertenece.
--     Ordenar los resultados por apellido de forma descendente.
SELECT DISTINCT
  e.apellido,e.oficio,d.dnombre 
FROM 
  emple e 
JOIN 
  depart d ON e.dept_no = d.dept_no 
ORDER BY 
  e.apellido DESC;

-- 31. Listar el número de empleados por departamento.
SELECT dept_no, COUNT(*) AS numero_de_empleados FROM emple GROUP BY dept_no;

-- 32. Realizar el mismo comando anterior pero obteniendo una salida como la que vemos.
SELECT d.dnombre, COUNT(*) AS numero_de_empleados 
FROM emple e JOIN depart d ON e.dept_no = d.dept_no GROUP BY d.dnombre;

-- 33. Listar el apellido de todos los empleados y ordenarlos por oficio, y por nombre.
SELECT DISTINCT e.apellido FROM emple e ORDER BY e.oficio,e.apellido;

-- 34. Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ.
--     Listar el apellido de los empleados.
SELECT DISTINCT apellido FROM emple WHERE apellido LIKE 'A%';

-- 35. Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ o por ’M’. 
--     Listar el apellido de los empleados
SELECT DISTINCT apellido FROM emple WHERE apellido LIKE 'A%' OR apellido LIKE 'M%';

-- 36. Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por ʻZʼ. 
--     Listar todos los campos de la tabla empleados
SELECT DISTINCT apellido FROM emple WHERE apellido NOT LIKE '%Z';

-- 37. Seleccionar de la tabla EMPLE aquellas filas cuyo APELLIDO empiece por ʻAʼ 
--     y el OFICIO tenga una ʻEʼ en cualquier posición. 
--     Ordenar la salida por oficio y por salario de forma descendente.
SELECT DISTINCT 
  e.apellido,e.oficio 
FROM 
  emple e 
WHERE 
  e.apellido LIKE 'A%' AND e.oficio LIKE '%E%' 
ORDER BY
  e.oficio DESC, e.salario DESC;
